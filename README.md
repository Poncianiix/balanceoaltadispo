# Practica de balanceo de cargas de alta disponibilidad 

La práctica implica crear cinco servidores diferentes (bastion, servidor 1, servidor 2, servidor 3 y servidor frontal) utilizando contenedores con imágenes de Ubuntu y NGINX. (Yo elegi ubuntu para no complicarme tanto como con debian)

El servidor frontal recibe las solicitudes de los clientes y las redirige a los servidores web. Los servidores web tienen múltiples instancias de node dentro de contenedores con imágenes de Node, cada uno de los cuales tiene su propio servidor frontal NGINX que dirige las solicitudes a cada instancia de node.

El servidor Bastion es responsable de controlar los otros servidores mediante un playbook de Ansible. 

## Como lo hice funcionar

Primero configure un archivo **(docker-compose.yml)** en donde creo los 5 servidores que la practica requiere. 
El primer servidor es el bastion el cual configure dejando el puerto 22 abierto y cambiando el password de root para poder acceder desde cualquier computadora.

```
bastion:
    tty: true
    build: 
      context: .
      dockerfile: DockerfileBas
    container_name: bastion-server
    hostname: bas
    privileged: true
    ports:
      - 22:22

```
También cree un archivo dockerfile para llamado **(DockerfileBas)** En este configure lo que se tenía que instalar en el servidor como python, git, ssh. Así mismo aqui fue en donde cambie la contraseña de root y active la conexión ssh de root por último genere la huella digital e inicie el servicio de ssh.

```
# Tomamos la imagen de ubuntu mas reciente
FROM ubuntu:latest
WORKDIR /root
# Instalamos SSH, Ansible,nano y Python3
RUN apt update 
RUN apt install -y openssh-server
RUN apt install -y ansible 
RUN apt install -y python3
RUN apt install -y nano



RUN mkdir /etc/ansible && touch /etc/ansible/hosts

#Reestablecemos el password del usuario root
RUN echo "root:123456" | chpasswd
# Eliminamos el archivo de configuracion de SSH
RUN rm /etc/ssh/sshd_config
# Creamos el archivo de configuracion de SSH para que nos permita conectarnos como root
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
# Creamos el archivo con las llaves publicas autorizadas
RUN ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ''
# Generamos nuestras llaves
RUN touch /root/.ssh/authorized_keys
# Abrimos el puerto 22 para SSH
EXPOSE 22
# Arrancamos el servicio SSH 
CMD service ssh start ; sleep infinity

```

Los siguientes fueron los 3 servidores los cuales también les configure el nombre server-1,server-2,server-3 un punto importante es el activar el privileged: true
ya que esto es lo que les va a permitir ejecutar docker.

```
  server-1:
    tty: true
    build: 
      context: .
      dockerfile: DockerfileServ
    container_name: server-1
    hostname: serv1
    privileged: true

```

Para estos servidores también configure un archivo dockerfile el cual se llama **(DockerfileServ)** aquí instalo todo lo necesario para su funcionamiento básico, también cambió la contraseña de root y activo el inicio de sesión de root. También creó el archivo authorized_keys y dejó iniciado el servicio ssh.

```
# Tomamos la imagen de ubuntu mas reciente
FROM ubuntu:latest
WORKDIR /root
# Instalamos SSH, Sudo, Python3, nano y git
RUN apt update 
RUN apt install -y openssh-server
RUN apt install -y python3
RUN apt install -y nano
RUN apt install -y sudo
RUN apt install -y git

RUN echo "root:123456" | chpasswd
RUN rm /etc/ssh/sshd_config

RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config

# Creamos el archivo con las llaves publicas autorizadas
RUN mkdir /root/.ssh && touch /root/.ssh/authorized_keys
# Arrancamos el servicio SSH
CMD service ssh start ; sleep infinity


```

Por ultimo configuramos el servidor frontal con NGINX

```
  balanceador:
    image: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    depends_on:
      - server-1
      - server-2
      - server-3

```
## Configuracion mediante ansible


Con la parte anterior quedo lo minimo para el funcionamiento de los servidores. Pero hace falta configurarlos, para eso primero me conecto a el server bastion por ssh:
```bash
ssh root@172.20.0.5
```

Esto me va a pedir un password que previamente configure.

Una vez que estemos dentro tenemos que configurar el archiv hosts quedando de la siguiente manaera:

```
localhost ansible_connection=local 

serv1 ansible_user=root
serv2 ansible_user=root
serv3 ansible_user=root 

 


```

Después le hice cat al id_rsa.pub y me conecte via ssh a cada servidor para agregarla en el archivo authorized_keys

```bash
ssh root@serv1
```
```bash
ssh root@serv2
```
```bash
ssh root@serv3
```

Con todos los servidores configurados lanze un comando ansible para comprobar que todo esté bien.

```bash
ansible all -a "ls"
```
Si todo salió correcto tengo que cubrir la brecha de seguridad que deje abierta al permitir el ssh a cada servidor. Para esto cree un archivo **(playbookSecurity.yml)** que su función es restablecer la contraseña de root y desactivar la conexión ssh de root.

```
- hosts: ['serv1', 'serv2', 'serv3']
  become: yes
  tasks:
  - name: Cambiamos el password a root 
    shell: 'echo "root:Maruchan" | chpasswd'
  - name: Eliminamos el acceso ssh
    shell: 'rm /etc/ssh/sshd_config'
  - name: Desactivamos el login por root
    shell: 'echo "PermitRootLogin no" >> /etc/ssh/sshd_config'


```

Ejecutamos el comando


```bash
ansible-playbook playbookSecurity.yml
```

Ya con todo esto podemos hacer el **(playbookDocker.yml)** el cual contiene lo necesario para instalar docker, clonar el repositorio git con la aplicación de balanceo y ejecutar docker compose en cada uno de ellos.

```
- hosts: ['serv1', 'serv2', 'serv3']
  become: yes
  tasks:
# Instalamos Curl
  - name: Instalamos curl
    apt:
      name: curl
      state: present
  - name: Instalamos Docker
    shell: 'curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh'

# Iniciamos docker 
  - name: Iniciamos el servicio de Docker por medio del shell
    shell: 'sudo service docker start'
    ignore_errors: yes

# Clonamos la apicacion de balanceo 
  - name: Clonamos el repositorio de la aplicación de balanceo
    git:
      repo: https://gitlab.com/Poncianiix/practicabalanceo.git
      dest: /home/balancing-app


# Ejecutamos el docker compose
- hosts: ['serv1', 'serv2', 'serv3']
  become: yes
  tasks:
  - name: Acceder a la carpeta con la aplicación de balanceo y ejecutar docker compose
    shell: 'cd /home/balancing-app && docker compose up -d'

```
Comando para ejecutar el playbook

```bash
ansible-playbook playbookDocker.yml
```

Por ultimo revisamos en el navegador en localhost y podemos observer el correcto funcionamiento.